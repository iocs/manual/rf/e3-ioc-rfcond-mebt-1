require essioc
require rfcond

epicsEnvSet("ENGINEER", "Gabriel Fedel <gabriel.fedel@ess.eu>")
## Add extra environment variables here
epicsEnvSet("TOP",      "$(E3_CMD_TOP)")

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

# General
epicsEnvSet("M", "MEBT-010")
epicsEnvSet("EVR", "MEBT-010:RFS-EVR-101:")
epicsEnvSet("LLRF", "MEBT-010:RFS-LLRF-101:")
epicsEnvSet("LLRFD", "MEBT-010:RFS-DIG-101:")
epicsEnvSet("FIM", "MEBT-010:RFS-FIM-101:")
epicsEnvSet("VAC1", "MEBT-010:Vac-VGC-10000:PrsR CPP")

iocshLoad("$(rfcond_DIR)/rfcond.iocsh")

# Call iocInit to start the IOC
iocInit()
date
